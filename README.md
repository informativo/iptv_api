# IPTV Manager
#### Installation
To proper install a IPTV Panel you need to install debian packages:
- php5
- php5-cli
- php5-fpm
- php5-intl
- php5-mysql
- php5-xsl
- mysql-server
- libssh2-php (that is an a php ssh extension)
- php5-curl

```sh
$ git clone https://informativo@bitbucket.org/informativo/iptv_api.git
$ cd iptv
$ apt-get install php5 php5-cli php5-fpm php5-ffmpeg php5-xsl php5-intl mysql-server php5-mysql php5-curl libssh2-php
```

#### Composer
```sh
$ chmod +x ./composer.phar
$ ./composer.phar install
```
You will be prompted for database settings, you need to provide some.
When you want to update core just run:
```sh
$ git pull --rebase
$ ./composer.phar update
```
#### Nginx and FPM OR built-in WebServer
nginx vhost conf

```sh
server {
    server_name domain.tld www.domain.tld;
    root /var/www/iptv/web;

    location / {
        # try to serve file directly, fallback to app.php
        try_files $uri /index.php$is_args$args;
    }
    
    # PROD
    location ~ ^/index\.php(/|$) {
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param HTTPS off;
        # Prevents URIs that include the front controller. This will 404:
        # http://domain.tld/app.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
    }

    error_log /var/log/nginx/project_error.log;
    access_log /var/log/nginx/project_access.log;
}
```

#### Develop custom modules
I can develop custom modules for that software specially for you. Ask me if you want something.
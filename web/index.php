<?php

// web/index.php
require_once __DIR__.'/../vendor/autoload.php';

$env = 'prod';

$app = new Silex\Application();

$app->register(new Igorw\Silex\ConfigServiceProvider(__DIR__."/../config/$env.json"));

$app->register(new FFMpeg\FFMpegServiceProvider(), array(
    'ffmpeg.configuration' => array(
        'ffmpeg.threads'   => $app['ffmpeg_threads'],
        'ffmpeg.timeout'   => $app['ffmpeg_timeout'],
        'ffmpeg.binaries'  => $app['ffmpeg_path'],
        'ffprobe.timeout'  => $app['ffmpeg_ffprobe_timeout'],
        'ffprobe.binaries' => $app['ffmpeg_ffprobe_path'],
    ),
));

/**
 * Catch screens sharing for Station outputs
 */
$app->get('/screenshot/{uri}', function (Silex\Application $app, $uri, \Symfony\Component\HttpFoundation\Request $request) {
    $vidUrl = base64_decode($uri);

    $path = 'ffmpeg/'.md5(time().md5($vidUrl)).'.jpg';
    $video = $app['ffmpeg']->open($uri);

    try {
        $video
            ->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(0))
            ->save($path);

        $content = base64_encode(file_get_contents($path));
        $return = [
            'content' => $content,
            'license' => $app['license']
        ];

        @unlink($path);
        return $app->json($return);
    }
    catch (Exception $e) {
        $return = [
            'error' => 'Channel is not available',
            'license' => $app['license']
        ];
        return $app->json($return);
    }
});

/**
 * Catch screens sharing for transcoder outputs
 */
$app->get('/transcoder_screen/{uri}', function (Silex\Application $app, $uri, \Symfony\Component\HttpFoundation\Request $request) {
    $vidUrl = base64_decode($uri);

    $path = 'ffmpeg/'.md5('transcode_'. time().md5($vidUrl)).'.jpg';
    $video = $app['ffmpeg']->open($uri);

    try {
        $video
            ->frame(\FFMpeg\Coordinate\TimeCode::fromSeconds(0))
            ->save($path);

        $content = base64_encode(file_get_contents($path));
        $return = [
            'content' => $content,
            'license' => $app['license']
        ];

        @unlink($path);
        return $app->json($return);
    }
    catch (Exception $e) {
        $return = [
            'error' => 'Channel is not available',
            'license' => $app['license']
        ];
        return $app->json($return);
    }

});

/**
 * Catch config upload for station
 */
$app->post('/config', function (Silex\Application $app, \Symfony\Component\HttpFoundation\Request $request) {

    $license = $request->get("integration_key");
    if ($license !== $app['integration_key']) {
        $app->abort(404, "Wrong request");
    }

    $data = $request->get('data');

    $adapters = base64_decode($data);
    $adapters = json_decode($adapters);

    foreach ($adapters as $name => $adapter) {
        file_put_contents($adapter->path, $adapter->contents);
    }

    $return = [
        'saved' => true,
        'license' => $app['license']
    ];

    return $app->json($return);
});

/**
 * Catch config upload for station
 */
$app->post('/analyze', function (Silex\Application $app, \Symfony\Component\HttpFoundation\Request $request) {

    $license = $request->get("integration_key");
    if ($license !== $app['integration_key']) {
        $app->abort(404, "Wrong request");
    }

    $data = $request->get('data');

    $config = base64_decode($data);
    $config = json_decode($config);

    $process = new \Symfony\Component\Process\Process($config->cmd);
    $process->start();

    sleep(10);

    $process->signal(SIGKILL);

    $return = [
        'output' => $process->getOutput(),
        'license' => $app['license']
    ];

    return $app->json($return);
});

/**
 * Catch config upload for station
 */
$app->post('/signal', function (Silex\Application $app, \Symfony\Component\HttpFoundation\Request $request) {

    $license = $request->get("integration_key");
    if ($license !== $app['integration_key']) {
        $app->abort(404, "Wrong request");
    }

    $data = $request->get("data");

    $config = base64_decode($data);
    $config = json_decode($config);

    $process = new \Symfony\Component\Process\Process($config->cmd);
    $process->start();

    while ($process->isRunning()) {
        //
    }

    $return = [
        'output' => $process->getOutput(),
        'license' => $app['license']
    ];

    return $app->json($return);
});

/**
 * Catch restart adapter action,
 * Restarts if current process exists.
 */
$app->post('/restart', function (Silex\Application $app, \Symfony\Component\HttpFoundation\Request $request) {

    $license = $request->get("integration_key");
    if ($license !== $app['integration_key']) {
        $app->abort(404, "Wrong request");
    }

    $data = $request->get("data");

    $config = base64_decode($data);
    $config = json_decode($config);

    // Check if file with extension exists
    if (false === file_exists($config->pid) || false === is_readable($config->pid)) {
        try {
            $process = new \Symfony\Component\Process\Process($config->start_command);
            $process->start();

            $started = true;
            while($started === true) {
                sleep(3);
                $started = false;
            }

        } catch (Exception $e) {}

        return $app->json([
            'started' => true,
            'stopped' => false,
            'license' => $app['license']
        ]);
    }

    if (true === file_exists($config->pid) && true === is_readable($config->pid)) {
        $pid = file_get_contents($config->pid);
        $cmd = 'kill -9 ' . $pid;

        try {
            $process = new \Symfony\Component\Process\Process($cmd);
            $process->start();
        }
        catch (Exception $e) {}
    }

    try {
        $process = new \Symfony\Component\Process\Process($config->start_command);
        $process->start();

        $started = true;
        while($started === true) {
            sleep(3);
            $started = false;
        }

    } catch (Exception $e) {}

    $return = [
        'started' => true,
        'stopped' => true,
        'license' => $app['license']
    ];
    return $app->json($return);
});

$app->run();


